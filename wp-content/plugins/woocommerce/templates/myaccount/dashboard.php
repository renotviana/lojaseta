<?php
/**
 * My Account Dashboard
 *
 * Shows the first intro screen on the account dashboard.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/dashboard.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
      $user_info = get_user_meta($current_user->ID);
?>
				<div class="container">
					<div class="row">	
					<div class="col-lg-6">
						<div class="card">
							<h2>Meu Perfil</h2>
							<p>Nome: <?=$current_user->user_firstname?></p>
							<p>E-mail: <?=$current_user->user_email?></p>
							<p>Data de Nascimento: <?=$user_info['n5ascimento'][0]?></p>
							<p>CPF: <?=$user_info['cpf'][0]?></p>
							<p>Telefone: <?=$user_info['telefone'][0]?></p>
							<a href="http://loja.sistemaseta.com.br/minha-conta/edit-account/"><button>Alterar Dados</button></a>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="card">
							<h2>Meus Endereços</h2>
							<?php
								if ($user_info['shipping_address_1'][0] == '') {
									echo "<p>Você ainda não cadastrou seu endereço</p>";
									echo '<a href="http://loja.sistemaseta.com.br/minha-conta/edit-address/entrega/"><button>Cadastrar Endereço</button></a>';
								}
								else{
							?>
							<p><?=$user_info['shipping_address_1'][0]?></p>
							<?php if($user_info['shipping_address_2'][0] != ''){ ?>
							<p>  <?=$user_info['shipping_address_2'][0]?></p>
							<?php 	} ?>
							<p><?=$user_info['shipping_city'][0]?>, <?=$user_info['shipping_state'][0]?></p>
							<p>CEP:<?=$user_info['shipping_postcode'][0]?></p>
							<a href="http://loja.sistemaseta.com.br/minha-conta/edit-address/entrega/"><button>Alterar Endereço</button></a>
							<?php 	} ?>
						</div
>					</div>
					</div>
				</div>


<?php
	/**
	 * My Account dashboard.
	 *
	 * @since 2.6.0
	 */
	do_action( 'woocommerce_account_dashboard' );

	/**
	 * Deprecated woocommerce_before_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_before_my_account' );

	/**
	 * Deprecated woocommerce_after_my_account action.
	 *
	 * @deprecated 2.6.0
	 */
	do_action( 'woocommerce_after_my_account' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
