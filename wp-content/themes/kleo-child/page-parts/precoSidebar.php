<div class="preco-sidebar">
	<h2>Preço</h2>
	<ul>
		<li><a href="./?max_price=30">Até R$ 30</a></li>
		<li><a href="./?min_price=30&max_price=50">R$30 - R$ 50</a></li>
		<li><a href="./?min_price=50&max_price=80">R$50 - R$ 80</a></li>
		<li><a href="./?min_price=80&max_price=100">R$80 - R$ 100</a></li>
		<li><a href="./?min_price=100">Acima de R$ 100</a></li>
	</ul>
</div>