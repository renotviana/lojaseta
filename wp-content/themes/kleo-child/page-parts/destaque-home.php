<?php  
    $args = array(
        'post_type'      => 'product',
        'posts_per_page' => 9,
        'orderby' => 'date',
        
    );

    $loop = new WP_Query( $args );




?>

<div class="row">
			<div class="col-lg-12">
				<h2>EM DESTAQUE</h2>
			</div>
		</div>
		<div class="row destaques">
			<?php 
			    while ( $loop->have_posts() ) : $loop->the_post();
        global $product; 
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );?>


       

		<div class="col-lg-4">
			<a href="<?=get_permalink()?>">
				<div class="product" >
					<div class="product-img" style="background-image: url(<?php  echo $image[0]; ?>);"></div>
					<h3 class="product-name text-uppercase"><a href="<?=get_permalink()?>"><?=get_the_title()?></a></h3>
					<p class="product-price"><?= $product->get_price_html(); ?></p>	
				</div>
				</a>
					
			</div>
		<?php
    endwhile;
    ?>
			
			<div class="col-lg-6">
				<div class="square">
					<p>10% de desconto
<span>nos produtos do</span>
seta heroes</p>
				</div>
			</div>
			<div class="col-lg-6">
				<a href="http://loja.sistemaseta.com.br/categoria-produto/camisa/">
				<div class="square camisa">
					<p>CONHEÇA
<span>NOSSA LINHA DE</span>
CAMISETAS!</p>
				</div>
			</a>
			</div>
		</div>