<?php
/**
 * Before content wrap
 * Used in all templates
 */
?>
<?php
$main_tpl_classes = apply_filters('kleo_main_template_classes', '');

if (kleo_has_shortcode('kleo_bp_')) {
	$section_id = 'id="buddypress" ';
}	else {
	$section_id = '';
}

$container = apply_filters('kleo_main_container_class','container');

/**
 * Before main content - action
 */
do_action('kleo_before_content');

?>
<?php $breadcrumb_data = kleo_breadcrumb(array(
                'show_browse' => false,
                'separator' => ' ',
                'show_home'  => __( 'Home', 'kleo_framework' ),
                'echo'       => false
            )); 
?>
<div class="bread-crumb">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-right"><?=$breadcrumb_data;?></div>
		</div>
	</div>
</div>
<section class="container-wrap main-color">
	<div id="main-container" class="<?php echo $container; ?>">
		<?php if($container == 'container') { ?><div class="row"> <?php } ?>

			<div <?php echo $section_id;?>class="template-page <?php echo $main_tpl_classes; ?>">
				<div class="wrap-content">
					
				<?php
				/**
				 * Before main content - action
				 */
				do_action('kleo_before_main_content');
				?>