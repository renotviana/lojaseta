<?php

  $taxonomy     = 'product_cat';
  $orderby      = 'name';  
  $show_count   = 0;      // 1 for yes, 0 for no
  $pad_counts   = 0;      // 1 for yes, 0 for no
  $hierarchical = 1;      // 1 for yes, 0 for no  
  $title        = '';  
  $empty        = 0;

  $args = array(
         'taxonomy'     => $taxonomy,
         'orderby'      => $orderby,
         'show_count'   => $show_count,
         'pad_counts'   => $pad_counts,
         'hierarchical' => $hierarchical,
         'title_li'     => $title,
         'hide_empty'   => $empty
  );
 $all_categories = get_categories( $args ); ?>
<div class="categorias">
	<h2>Categorias</h2>
	<ul>
		<?php foreach ($all_categories as $categoria) { ?>
			<li><a href="http://loja.sistemaseta.com.br/categoria-produto/<?=$categoria->slug?>"><?=$categoria->name?></a></li>
		<?php } ?>
	</ul>
</div>