<?php
$taxonomy = 'product_cat';
$orderby = 'name';
$order = 'ASC';
$show_count = 0; // 1 for yes, 0 for no
$pad_counts = 0; // 1 for yes, 0 for no
$hierarchical = 1; // 1 for yes, 0 for no
$title = '';
$empty = 1;

$args = array(
    'taxonomy' => $taxonomy,
    'orderby' => $orderby,
    'order' => $order,  
    'show_count' => $show_count,
    'pad_counts' => $pad_counts,
    'hierarchical' => $hierarchical,
    'title_li' => $title,
    'hide_empty' => $empty,
    'parent' => 0
);

$all_categories = get_categories( $args );


?>

<ul>
	<?php 	foreach ($all_categories as  $categoria) { ?>
	<a href="http://loja.sistemaseta.com.br/categoria-produto/<?=$categoria->slug?>"><li><?=$categoria->name?></li></a>
	<?php } ?>
</ul>