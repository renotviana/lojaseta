<?php global $current_user;
      get_currentuserinfo();
      $user_info = get_user_meta($current_user->ID);

?>

<div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1>Bem Vindo, <?=$current_user->user_firstname?></h1>
		</div>
		<div class="col-lg-3">
			
		</div>
		<div class="col-lg-9">
			<div class="row">
				<div class="container">
					<div class="col-lg-6">
						<div class="card">
							<h2>Meu Perfil</h2>
							<p>Nome: <?=$current_user->user_firstname?></p>
							<p>E-mail: <?=$current_user->user_email?></p>
							<p>Data de Nascimento: <?=$user_info['n5ascimento'][0]?></p>
							<p>CPF: <?=$user_info['cpf'][0]?></p>
							<p>Telefone: <?=$user_info['telefone'][0]?></p>
							<button>Alterar Dados</button>
						</div>
					</div>
					<div class="col-lg-6">
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>