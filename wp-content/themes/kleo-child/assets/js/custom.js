jQuery('#cadastro input[type=submit]').on('click', function(event) {
    event.preventDefault();
    jQuery('.alert-success , .alert-danger').hide()
    jQuery.ajax({
            url: '',
            type: 'POST',
            data: jQuery('#cadastro').serialize()
        })
        .done(function(msg) {
            if (msg == 'true') {

                jQuery('.alert-success').show()

            } else {
                jQuery('.alert-danger').show()
            }
        })
});