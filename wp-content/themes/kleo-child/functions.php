<?php
/**
 * @package WordPress
 * @subpackage Kleo
 * @author SeventhQueen <themesupport@seventhqueen.com>
 * @since Kleo 1.0
 */

/**
 * Kleo Child Theme Functions
 * Add custom code below
*/ 

  add_filter( 'wp_nav_menu_items', 'getLogin', 201, 2 );

  function getLogin ( $items, $args )
  {
        if ( sq_option( 'ajax_search', 1 ) == 'logged_in' && ! is_user_logged_in() ) {
            return $items;
        }
    
    $location = sq_option( 'menu_search_location', 'primary' );

      if ( $args->theme_location == $location )
      {
          $form = getLoginx();
          $items .= '<li id="login" class="">' . $form . '</li>';
      }
      return $items;
  }


function getLoginx() {
    ob_start();
    ?>
    <a class="" href="http://loja.sistemaseta.com.br/admin"><i class="fa fa-user" aria-hidden="true"></i>Login</a>
    
    <?php
    $form = ob_get_clean();
    return $form;
}
  add_filter( 'wp_nav_menu_items', 'cadastrar', 202, 2 );

  function cadastrar ( $items, $args )
  {
        if ( sq_option( 'ajax_search', 1 ) == 'logged_in' && ! is_user_logged_in() ) {
            return $items;
        }
    
    $location = sq_option( 'menu_search_location', 'primary' );

      if ( $args->theme_location == $location )
      {
          $form = cadastrarx();
          $items .= '<li id="cadastrar" class="">' . $form . '</li>';
      }
      return $items;
  }


function cadastrarx() {
    ob_start();
    ?>
    <a class="" href="http://loja.sistemaseta.com.br/cadastro"><i class="fa fa-pencil" aria-hidden="true"></i></i>Cadastrar</a>
    
    <?php
    $form = ob_get_clean();
    return $form;
}

add_shortcode( 'destaque-home', 'emDestaque' );
add_shortcode( 'categorias-sidebar', 'categoriaSidebar' );
add_shortcode( 'preco-sidebar', 'precoSidebar' );
add_shortcode( 'perfil', 'getPerfil' );
add_shortcode( 'genero-sidebar', 'generoSidebar' );
add_shortcode( 'footerCategoria', 'footercat' );
add_shortcode( 'footer-3', 'footer3' );

if (isset($_GET['qtproduto'])) {
  add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

  function new_loop_shop_per_page( $cols ) {
    // $cols contains the current number of products per page based on the value stored on Options -> Reading
    // Return the number of products you wanna show per page.
    $cols = $_GET['qtproduto'];
    return $cols;
  }  
}
else{
  add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 20 );

  function new_loop_shop_per_page( $cols ) {
    // $cols contains the current number of products per page based on the value stored on Options -> Reading
    // Return the number of products you wanna show per page.
    $cols = 9;
    return $cols;
  }  
}

//enqueues our locally supplied font awesome stylesheet
function enqueue_our_required_stylesheets(){
  wp_enqueue_style('font-awesome', get_stylesheet_directory_uri() . '/assets/css/font-awesome.css'); 
}

add_action('wp_enqueue_scripts','enqueue_our_required_stylesheets');

function emDestaque(){
  get_template_part('page-parts/destaque-home');
} 

function footercat(){
  get_template_part('page-parts/footerCategoria');
} 

function footer3(){
	get_template_part('page-parts/footer3');
} 

function categoriaSidebar(){
  get_template_part('page-parts/categoriaSidebar'); 
}
function tabproduto(){
  get_template_part('page-parts/tabproduto'); 
}
function getPerfil(){
	get_template_part('page-parts/perfil');	
}
function precoSidebar(){
	get_template_part('page-parts/precoSidebar');	
}

function generoSidebar(){
  get_template_part('page-parts/generoSidebar'); 
}
function wp_custom_breadcrumbs() {
 
  $showOnHome = 0; // 1 - show breadcrumbs on the homepage, 0 - don't show
  $delimiter = '&raquo;'; // delimiter between crumbs
  $home = 'Home'; // text for the 'Home' link
  $showCurrent = 1; // 1 - show current post/page title in breadcrumbs, 0 - don't show
  $before = '<span class="current">'; // tag before the current crumb
  $after = '</span>'; // tag after the current crumb
 
  global $post;
  $homeLink = get_bloginfo('url');
 
  if (is_home() || is_front_page()) {
 
    if ($showOnHome == 1) echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a></div>';
 
  } else {
 
    echo '<div id="crumbs"><a href="' . $homeLink . '">' . $home . '</a> ' . $delimiter . ' ';
 
    if ( is_category() ) {
      $thisCat = get_category(get_query_var('cat'), false);
      if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
      echo $before . 'categoria "' . single_cat_title('', false) . '"' . $after;
 
    } elseif ( is_search() ) {
      echo $before . 'Search results for "' . get_search_query() . '"' . $after;
 
    } elseif ( is_day() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('d') . $after;
 
    } elseif ( is_month() ) {
      echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a> ' . $delimiter . ' ';
      echo $before . get_the_time('F') . $after;
 
    } elseif ( is_year() ) {
      echo $before . get_the_time('Y') . $after;
 
    } elseif ( is_single() && !is_attachment() ) {
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
        if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
        if ($showCurrent == 0) $cats = preg_replace("#^(.+)\s$delimiter\s$#", "$1", $cats);
        echo $cats;
        if ($showCurrent == 1) echo $before . get_the_title() . $after;
      }
 
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      echo $before . $post_type->labels->singular_name . $after;
 
    } elseif ( is_attachment() ) {
      $parent = get_post($post->post_parent);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
      echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_page() && !$post->post_parent ) {
      if ($showCurrent == 1) echo $before . get_the_title() . $after;
 
    } elseif ( is_page() && $post->post_parent ) {
      $parent_id  = $post->post_parent;
      $breadcrumbs = array();
      while ($parent_id) {
        $page = get_page($parent_id);
        $breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
        $parent_id  = $page->post_parent;
      }
      $breadcrumbs = array_reverse($breadcrumbs);
      for ($i = 0; $i < count($breadcrumbs); $i++) {
        echo $breadcrumbs[$i];
        if ($i != count($breadcrumbs)-1) echo ' ' . $delimiter . ' ';
      }
      if ($showCurrent == 1) echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
 
    } elseif ( is_tag() ) {
      echo $before . 'Posts tagged "' . single_tag_title('', false) . '"' . $after;
 
    } elseif ( is_author() ) {
       global $author;
      $userdata = get_userdata($author);
      echo $before . 'Articles posted by ' . $userdata->display_name . $after;
 
    } elseif ( is_404() ) {
      echo $before . 'Error 404' . $after;
    }
 
    if ( get_query_var('paged') ) {
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
      echo __('Page') . ' ' . get_query_var('paged');
      if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
    }
 
    echo '</div>';
 
  }
} // end wp_custom_breadcrumbs()

function create_account(){
    //You may need some data validation here
    $user = ( isset($_POST['emailcadastro']) ? $_POST['emailcadastro'] : '' );
    $pass = ( isset($_POST['senhacadastro']) ? $_POST['senhacadastro'] : '' );
    $email = ( isset($_POST['emailcadastro']) ? $_POST['emailcadastro'] : '' );

    if ( !username_exists( $user )  && !email_exists( $email ) ) {
       $user_id = wp_create_user( $user, $pass, $email );
       if( !is_wp_error($user_id) ) {
           //user has been created
           $user = new WP_User( $user_id );
           $user->set_role( 'customer' );
           update_user_meta($user_id, 'cpf', $_POST['cpfcadastro']);
           update_user_meta($user_id, 'nascimento', $_POST['datacadastro']);
           update_user_meta($user_id, 'first_name', $_POST['nomecadastro']);
           update_user_meta($user_id, 'telefone', $_POST['telcadastro']);
           //Redirects
           echo "true";
           exit;
       } else {
           //$user_id is a WP_Error object. Manage the error
       }
    }

}
if (isset($_POST['emailcadastro'])) {
  create_account();
}

function add_contact_methods($profile_fields) {
 
// Sintaxe $profile_fields['nome do campo'] = 'Descrição do campo';
$profile_fields['cpf'] = 'CPF';
$profile_fields['nascimento'] = 'Data de Nascimento';
$profile_fields['telefone'] = 'Telefone';

 
return $profile_fields;
}
add_filter('user_contactmethods', 'add_contact_methods');