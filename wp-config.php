<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações
// com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'loja');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'YalTu>U-kC+%3f=__}=XyMLut8Vtpp,Y!agu&]YA6in`e!<R~PH3<q-k&skl,ma]');
define('SECURE_AUTH_KEY',  ',q-,JV]UQ{EsZPd9+RjaY@.:g`>2$|7Ie$}Sm1Cb~a-MT6I+P[Phe:~OxhjQy{/h');
define('LOGGED_IN_KEY',    'M3KivWQ$ShHMW#U9)Z<X`k#7}v9)=J:s]!^ z}tAD71Lv!*pX6@de#19X{@o<v&U');
define('NONCE_KEY',        'N&>A6=zXK7,n/vBr2htzSe@)[(*o|Y99X{m-Z$$T5dTk;$nI{8SJp!:90uxN}5[A');
define('AUTH_SALT',        'DKX558$O*,Y(bCftjgYZpJ}) Q5S]!Ps]be7Vuag06QqsbfRmvf|$?(ceV2ZMOH2');
define('SECURE_AUTH_SALT', '5Tr:lj@arDxOi#>}[`N46Bq-<c-z }>peeD8Axr~G5GD>MrngTy74*d*;[]>WT-~');
define('LOGGED_IN_SALT',   'h}|J/ZTnJN=+>DQ-A,|R}FQ%D+#oy2U#,:Yn;z*=|2akKza+;)HZ+n~?GToDxMqp');
define('NONCE_SALT',       '<dh~)z>D}9YK;JgW=9F9zFK~b1k}>?GNVAnMd*q8=Z32{-<BR@S(.+<N-`Ygjd,X');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
